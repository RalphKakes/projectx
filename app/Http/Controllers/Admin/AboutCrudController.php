<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AboutRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AboutCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AboutCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\About');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/about');
        $this->crud->setEntityNameStrings('about', 'about');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
//        $this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'id',
            'label' => trans('id'),
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'about',
            'label' => trans('about'),
        'type' => 'text',
        ]);






    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AboutRequest::class);



        $this->crud->addField([
            'name' => 'about',
            'label' => trans('about'),
            'type' => 'wysiwyg',
        ]);


        

        // TODO: remove setFromDb() and manually define Fields
//        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
