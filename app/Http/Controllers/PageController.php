<?php

namespace App\Http\Controllers;

use App\Models\about;
use Illuminate\Http\Request;
use App\Models\Data;
use App\Models\users;
use App\Models\darkmode;


class PageController extends Controller
{

    public function updateStatus(Request $request)
    {
        $switch = darkmode::findOrFail('1');
        $switch->switch = $request->switch;
        $switch->save();

        return response()->json(['message' => 'Switch status updated successfully.']);
    }


    public function CallData()
    {
        $this->viewdata = array(
            'data' => Data::all(),
            'about' => about::all(),
            'darkmode' => darkmode::select('switch')->groupBy('id')->first()

    );
        return view ('pages.home', $this->viewdata);
    }

    public function CallDataRalph()
    {
        $this->viewdata = array(
            'data' => Data::all()
        );
        return view ('pages.ralph', $this->viewdata);
    }

    public function CallDataMike()
    {
        $this->viewdata = array(
            'users' => users::all()
        );
        return view ('pages.mike', $this->viewdata);
    }






}
