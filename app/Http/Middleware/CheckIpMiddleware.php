<?php
namespace App\Http\Middleware;
use Closure;
class CheckIpMiddleware
{
    public $whiteIps = ['92.109.55.13'];
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (env('APP_ENV') === 'test') {
            if (!in_array($request->ip(), $this->whiteIps)) {
                /*                You can redirect to any error page.            */
                return response()->json('Your ip address is not valid.');
            }
        }
            return $next($request);
        }

}