@if($darkmode->switch == 1)
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Ralph Kakes</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse " id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger " href="#about">{{trans('menu.About')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#projects">{{trans('menu.Projects')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#signup">Contact</a>
            </li>



            <div class="dropdown">
            <li class="nav-item dropdown">
                <li  class="nav-link js-scroll-trigger bg-transparent dropdown-toggle" href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Themes</li>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">{{trans('menu.Dark')}}</a>
                    <a class="dropdown-item" href="#">{{trans('menuLight')}}</a>
                </div>
            </li>
            </div>
            <li class="nav-item">
                <a href="{{ LaravelLocalization::getLocalizedURL('nl_NL') }}"> <svg xmlns="http://www.w3.org/2000/svg" width="48" height="32" viewBox="0 0 9 6">
                        <rect fill="#21468B" width="9" height="6"/>
                        <rect fill="#FFF" width="9" height="4"/>
                        <rect fill="#AE1C28" width="9" height="2"/>
                    </svg>
                        </a>
                        <br>
                <a href="{{ LaravelLocalization::getLocalizedURL('en_EN') }}"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 30" width="48" height="32">
                        <clipPath id="t">
                            <path d="M30,15 h30 v15 z v15 h-30 z h-30 v-15 z v-15 h30 z"/>
                        </clipPath>
                        <path d="M0,0 v30 h60 v-30 z" fill="#00247d"/>
                        <path d="M0,0 L60,30 M60,0 L0,30" stroke="#fff" stroke-width="6"/>
                        <path d="M0,0 L60,30 M60,0 L0,30" clip-path="url(#t)" stroke="#cf142b" stroke-width="4"/>
                        <path d="M30,0 v30 M0,15 h60" stroke="#fff" stroke-width="10"/>
                        <path d="M30,0 v30 M0,15 h60" stroke="#cf142b" stroke-width="6"/>
                    </svg>
                </a>
            </li>
        </ul>
    </div>
    </div>

</nav>
@else
    <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Ralph Kakes</a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
            <div class="collapse navbar-collapse " id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger " href="#about">{{trans('About')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#projects">{{trans('Projects')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#signup">Skills</a>
                    </li>
                    <li class="nav-item" style="margin-top: 17px;">
                        <a style="color: black" href="{{ LaravelLocalization::getLocalizedURL('nl_NL') }}"> <h6>NL</h6></a>
                        <a style="color: black" href="{{ LaravelLocalization::getLocalizedURL('en_EN') }}"> <h6>EN</h6>
                        </a>
                    </li>
                </ul>
            </div>
    </div>


</nav>
    @endif

