
<section id="signup" class="signup-section">
            <div class="container">
                <h1 class="text-white mb-5" style="text-align: center">Skills</h1>

                <div class="row">

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-html5 text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">HTML</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-css3-alt text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">CSS</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-js-square text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">JavaScript</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-php text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">PHP</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-vuejs text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">VueJS</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3 mb-md-2">
                        <div class="card py-4 h-100">
                            <div class="card-body text-center">
                                <i class="fab fa-laravel text-primary mb-2"></i>
                                <h4 class="text-uppercase m-0">Laravel</h4>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section class="contact-section bg-black">

        <div class="d-flex justify-content-center">
            <a href="mailto:ralphkakes@gmail.com" class="mx-2">
                <i class="fas fa-envelope"></i>
            </a>
            <a href="https://www.linkedin.com/in/ralph-kakes/" target="_blank" class="mx-2">
                <i class="fab fa-linkedin"></i>
            </a>
            <a href="https://github.com/RalphKakes" target="_blank" class="mx-2">
                <i class="fab fa-github"></i>
            </a>
        </div>

</section>
