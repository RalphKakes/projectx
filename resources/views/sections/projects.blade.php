@if($darkmode->switch == 1)
<section id="projects" class="projects-section bg-light">
    <div class="container">

        <!-- Featured Project Row -->
        <div class="row align-items-center no-gutters mb-4 mb-lg-5">
            <div class="col-xl-8 col-lg-7">
                <img class="img-fluid mb-3 mb-lg-0" src="img/bg-masthead.jpg" alt="">
            </div>
            <div class="col-xl-4 col-lg-5">
                <div class="featured-text text-center text-lg-left">
                    <h4>Yeet</h4>
                    <p class="text-black-50 mb-0">
                        @foreach($data as $datas)
                            {{$datas->data}}
                        @endforeach
                       Test</p>
                </div>
            </div>
        </div>

        <!-- Project One Row
        <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
            <div class="col-lg-6">
                <img class="img-fluid" src="img/demo-image-01.jpg" alt="">
            </div>
            <div class="col-lg-6">
                <div class="bg-black text-center h-100 project">
                    <div class="d-flex h-100">
                        <div class="project-text w-100 my-auto text-center text-lg-left">
                            <h4 class="text-white">Misty</h4>
                            <p class="mb-0 text-white-50">Test</p>
                            <hr class="d-none d-lg-block mb-0 ml-0">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Project Two Row -->
        <div class="row justify-content-center no-gutters">
            <div class="col-lg-6">
                <img class="img-fluid" src="img/demo-image-02.jpg" alt="">
            </div>
            <div class="col-lg-6 order-lg-first">
                <div class="bg-black text-center h-100 project">
                    <div class="d-flex h-100">
                        <div class="project-text w-100 my-auto text-center text-lg-right">
                            <h4 class="text-white">Mountains</h4>
                            <p class="mb-0 text-white-50">Test</p>
                            <hr class="d-none d-lg-block mb-0 mr-0">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
    -->
    @else
    <section id="projects" class="projects-section bg-light">
        <h1 style="text-align: center; padding-bottom: 50px;">{{trans('menu.Projecten')}}</h1>
        <div class="container">

            <!-- Featured Project Row -->
            <div class="row align-items-center no-gutters mb-4 mb-lg-5">
                <div class="col-xl-8 col-lg-7">
                    <a href="https://www.reedijkgroup.com/nl_NL" target="_blank">
                    <img class="img-fluid mb-3 mb-lg-0" style="" src="https://test.ralphkakes.nl/uploads/reedijkgroup.png" alt="">
                    </a>
                </div>
                <div class="col-xl-4 col-lg-5">
                    <div class="featured-text text-center text-lg-left">
                        <h4><a href="https://www.reedijkgroup.com/nl_NL" target="_blank">Reedijkgroup</a></h4>
                        <p class="text-black-50 mb-0">
                            @foreach($data as $datas)
                                {{$datas->data}}
                            @endforeach
                                {{trans('menu.Reedijkproject')}}</p>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                <div class="col-lg-12">
                    <div class="bg-black text-center h-100 project">
                        <div class="d-flex h-100">
                            <div class="project-text w-100 my-auto text-center text-lg-left">
                                <h1 class="text-white">"</h1>
                                <p class="text-white">{{trans('menu.Mikereview')}}</p>
                                <h1 class="text-white">"</h1>
                                <p class="mb-0 text-white-50">Mike Chan -- IT Manager Reedijk Wheeles and Tyres Group B.V.</p>
                                <hr class="d-none d-lg-block mb-0 ml-0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    <!--
            <div class="row justify-content-center no-gutters">
                <div class="col-lg-6">
                    <img class="img-fluid" src="img/demo-image-02.jpg" alt="">
                </div>
                <div class="col-lg-6 order-lg-first">
                    <div class="bg-black text-center h-100 project">
                        <div class="d-flex h-100">
                            <div class="project-text w-100 my-auto text-center text-lg-right">
                                <h4 class="text-white">Mountains</h4>
                                <p class="mb-0 text-white-50">Test</p>
                                <hr class="d-none d-lg-block mb-0 mr-0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->

        </div>
    </section>
    @endif