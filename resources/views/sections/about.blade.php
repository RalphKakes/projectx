<section id="about" class="about-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class="text-white mb-4">{{trans('menu.Hey im Ralph')}}</h2>
                <p class="text-white-50">{{trans('menu.Aboutralph')}}</p>
            </div>
        </div>
        <img src="https://test.ralphkakes.nl/uploads/smallralphkakes.jpeg" style="border-radius: 50%; padding-bottom: 10px;" alt="">
        <!-- <h2>{{trans('menu.Educatie')}}</h2> -->
    </div>
</section>