

@extends('layout.master')

@include('layout.nav')

@include('sections.header')


@section('content')
{{--    <input type="checkbox" data-id="1" name="status" class="js-switch" {{ $darkmode->switch == 1 ? 'checked' : '' }}>--}}

<!-- About Section -->
@include('sections.about')

<!-- Projects Section -->
@include('sections.projects')

<!-- Signup Section -->
@include('sections.contact')
<!-- Footer -->
    <script>
        let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

        elems.forEach(function(html) {
            let switchery = new Switchery(html,  { size: 'small' });
        });



        $(document).ready(function(){
            $('.js-switch').change(function () {
                let status = $(this).prop('checked') === true ? 1 : 0;
                let id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '{{ route('switch.update.status') }}',
                    data: {'switch': status, '1': id},
                    success: function (data) {
                        console.log(data.message);
                        location.reload();
                    }
                });
            });


        });
    </script>
<!-- Bootstrap core JavaScript -->
@endsection

