<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['checkIp'])->group(function () {

    Route::group(['prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['web', 'localeSessionRedirect', 'localize', 'localizationRedirect', 'localeViewPath']], function() {

    Route::get('/', function () {
        return view('home');
    });

    Route::get('/status/update', 'PageController@updateStatus')->name('switch.update.status');

    Route::get( "/" , "PageController@CallData");

    Route::get( "/ralph" , "PageController@CallDataRalph");

    Route::get("/mike","PageController@CallDataMike");

    Route::get("/blabla","PageController@CallData");


        /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/

    });

});